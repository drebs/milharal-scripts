#!/bin/sh
#
# Extract and display useful information from certbot logs.


set -eu


LETSENCRYPT_LOGS_PATH=/var/log/letsencrypt


_usage() {
  status=${1}
  echo "Usage: $0 [new|count]"
  exit ${status}
}


_new() {
  grep -R "POST /acme/new-cert" ${LETSENCRYPT_LOGS_PATH} \
    | sed -e "s/[^:]\+://" \
    | sort -r
}


_count() {
  echo "Certbot certificate creation attempts"
  echo "====================================="
  echo ""
  new=$(_new)
  dates=$(printf "${new}" | cut -d" " -f 1 | sort -r | uniq)
  for d in ${dates}; do
    created=$(printf "${new}" | grep ^${d} | grep "201 [0-9]\+\$" | wc -l)
    toomany=$(printf "${new}" | grep ^${d} | grep "429 [0-9]\+\$" | wc -l)
    echo "${d}:"
    echo "  Created (201): ${created}"
    echo "  Failed  (429): ${toomany}"
    echo ""
  done
}


_main() {
  if [ ${#} -ne 1 ]; then _usage 1; fi
  action=${1}
  case ${action} in
    new)
      _new
      ;;
    count)
      _count
      ;;
    *)
      _usage 1
      ;;
  esac
}


_main ${*}
