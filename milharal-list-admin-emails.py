#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2015 milharal.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import MySQLdb
import re


# locale configuration


WP_CONFIG_FILE = "/var/sites/milharal/site/wp-config.php"

# database-related functions

def _get_db_info():
    info = {}
    p = re.compile("define\('DB_(HOST|NAME|USER|PASSWORD)', '([^']+)'\);")
    with open(WP_CONFIG_FILE, "r") as f:
        for l in f.readlines():
            m = p.match(l)
            if m:
                name = m.groups()[0].lower()
                value = m.groups()[1]
                info[name] = value
    return info


def _get_cursor():
    info = _get_db_info()
    db = MySQLdb.connect(
        host=info['host'],
        user=info['user'],
        passwd=info['password'],
        db=info['name'])
    return db.cursor()


def _get_emails():
    query = """
    SELECT
      wp_users.user_email
    FROM
      wp_users,
      wp_blogs,
      wp_usermeta
    WHERE
      wp_users.ID = wp_usermeta.user_id
      AND wp_usermeta.meta_key = CONCAT('wp_', wp_blogs.blog_id,'_capabilities')
      AND wp_usermeta.meta_value LIKE '%administrator%'
      AND wp_blogs.deleted = '0'
    """
    c = _get_cursor()
    c.execute(query)
    return c.fetchall()


# and the program

if __name__ == "__main__":
    result = _get_emails()
    for email in result:
        print email[0]
