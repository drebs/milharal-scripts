#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2015 milharal.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import re
import MySQLdb

from email.mime.text import MIMEText
from subprocess import Popen, PIPE


WP_CONFIG_FILE = "/var/sites/milharal/site/wp-config.php"
ADMIN_ADDRESS = "milharal@lists.riseup.net"


class BlogNotFoundError(Exception):
    pass


# database-related functions

def _get_db_info():
    info = {}
    p = re.compile("define\('DB_(HOST|NAME|USER|PASSWORD)', '([^']+)'\);")
    with open(WP_CONFIG_FILE, "r") as f:
        for l in f.readlines():
            m = p.match(l)
            if m:
                name = m.groups()[0].lower()
                value = m.groups()[1]
                info[name] = value
    return info


def _get_cursor():
    info = _get_db_info()
    db = MySQLdb.connect(
        host=info['host'],
        user=info['user'],
        passwd=info['password'],
        db=info['name'])
    return db.cursor()


# mail-related functions

def _print_mail(address, subject, body, cc=""):
        print "-" * 78
        print "From: %s" % ADMIN_ADDRESS
        print "To: %s" % address
        if cc:
            print "Cc: %s" % cc
        print "Subject: %s" % subject
        print ""
        print body
        print "-" * 78


def send_mail(address, subject, body, cc="", from_addr=None):
    msg = MIMEText(body, "plain", "utf-8")
    if not from_addr:
        from_addr = ADMIN_ADDRESS
    msg["From"] = from_addr
    msg["To"] = address
    if cc:
        msg["Cc"] = cc
    msg["Subject"] = subject
    _send_msg(msg)


def _send_msg(msg):
    p = Popen(["/usr/sbin/sendmail", "-t"], stdin=PIPE)
    p.communicate(msg.as_string())


def get_blog_info(domain):
    c = _get_cursor()
    query = """
        SELECT blog_id
        FROM wp_blogs
        WHERE domain = '%s'
    """ % domain
    c.execute(query)
    blog_id = None
    try:
        blog_id = c.fetchall()[0][0]
    except IndexError:
        raise BlogNotFoundError
    query = """
        SELECT
          wp_users.user_email,
          wp_{blog_id}_options.option_value
        FROM
          wp_users,
          wp_usermeta,
          wp_blogs,
          wp_{blog_id}_options
        WHERE
          wp_users.ID = wp_usermeta.user_id
          AND wp_usermeta.meta_key = 'wp_{blog_id}_capabilities'
          AND wp_usermeta.meta_value LIKE '%administrator%'
          AND wp_blogs.blog_id = {blog_id}
          AND wp_{blog_id}_options.option_name = 'blogname'
    """.format(blog_id=blog_id)
    c.execute(query)
    result = c.fetchall()[0]
    address, name = result
    return address, name, int(blog_id)
