#!/bin/sh

WP_ROOT=/var/sites/milharal/wordpress

cd ${WP_ROOT}
wp --allow-root theme update --all
wp --allow-root plugin update --all
wp --allow-root core update
wp --allow-root core update-db
chown -R milharal:www-data ${WP_ROOT}
