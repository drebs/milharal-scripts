#!/bin/bash
#
# Request and configure a Let's Encrypt SSL certificate for every
# registered blog in the Milharal Network that does not have it yet.
#
# Let's Encrypt does not support wildcard SSL certificates and limits
# to 100 the number of domains (and subdomains) in a same certificate
# so we must create a certificate per blog.
#
# TODO: this setup should be done somehow as soon the user request the
#       blog otherwise her will get a SSL error page.
#
#       There is a Wordpress plugin that provides support to Let's
#       Encrypt (name?) but at the present date (Nov 2016) it is not
#       working properly (I don't remember the reason).  -semente
#
# TODO: accessing https://doesnotexist.milharal.org won't work if
#       *.milharal.org is not using wildcard SSL
#       certificate. Currently we are not using Let's Encrypt for
#       (www.)milharal.org. That is the only (known) reason we are not
#       using Let's Encrypt on our main domain. The only fix for this
#       I guess is to set Apache to not serve placeholder pages to
#       unregistered blogs and give a DNS error instead ("name not
#       resolved/DNS address not found").  -semente
#

DOMAINS=`cd /var/sites/milharal/wordpress && wp --allow-root site list --field=domain`

for domain in $DOMAINS; do
    if [ ! -d /etc/letsencrypt/live/$domain ]; then
	certbot certonly \
		--non-interactive \
		--agree-tos \
		--webroot \
		--webroot-path=/var/sites/milharal/wordpress \
		--domain=$domain \
		--email=milharal@lists.riseup.net \
	    || { echo "$0: An error has ocurred, skipping request of new certificates"; break; }
    fi
done

> /etc/apache2/sites-available/milharal-network.conf
for domain in $DOMAINS; do
    if [ $domain = "milharal.org" ]; then
	# milhara.org site is configured at /etc/apache2/sites-available/milharal.conf
	continue
    fi

    # use a letsencrypt certficate only if it exists for the domain
    if [ -d /etc/letsencrypt/live/$domain ]; then
	ssldir=/etc/letsencrypt/live/$domain
    else
	ssldir=/etc/apache2/ssl/milharal.org
    fi
    echo "Use MilharalBlog $domain $ssldir" >> /etc/apache2/sites-available/milharal-network.conf
done

service apache2 reload
