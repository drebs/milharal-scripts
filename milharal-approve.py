#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2015 milharal.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# This script does the following:
#
#   1. activate the blog.
#   2. subscribe blog owner address to mailing list
#   3. send message to blog owner
#   4. send message to milharal-vizinhanca

import argparse
import os

from subprocess import call

from util import send_mail
from util import get_blog_info, BlogNotFoundError


BASEDIR = os.path.dirname(os.path.realpath(__file__))


def _activate_blog(blog_id):
    admin_url = "http://www.milharal.org"
    path = "/var/sites/milharal/site"
    cmd = [
        "wp",
        "--allow-root",
        "--url=%s" % admin_url,
        "--path=%s" % path,
        "site", "activate", str(blog_id),
    ]
    call(cmd)


def _subscribe_address_to_list(subscriber, list_admin):
    address = "sympa@lists.riseup.net"
    subject = ""
    body = "ADD milharal-vizinhanca %s" % subscriber
    send_mail(address, subject, body, from_addr=list_admin)


def _inform_owner(address, domain, name):
    subject = "[milharal] site ativado: %s" % domain
    with open(os.path.join(BASEDIR, "tpl", "newblog-owner.txt")) as f:
        body = f.read() % (domain, name, address)
    send_mail(address, subject, body)


def _inform_lists(address, domain, name):
    subject = "novo site no milharal: %s" % domain
    with open(os.path.join(BASEDIR, "tpl", "newblog-list.txt")) as f:
        body = f.read() % (domain, name, address)
    send_mail(
        "milharal-vizinhanca@lists.riseup.net", subject, body,
        cc="milharal@lists.riseup.net")


def _parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "blog",
        help="the blog to be activated")
    parser.add_argument(
        "list_admin",
        help="the email address of the list admin")
    return parser.parse_args()


if __name__ == "__main__":
    args = _parse_args()
    domain = args.blog + ".milharal.org"
    try:
        address, name, blog_id = get_blog_info(domain)
    except BlogNotFoundError:
        print 'blog not found: %s' % domain
        exit(1)
    # 1. activate the blog
    _activate_blog(blog_id)
    # 2. subscribe blog owner address to mailing list
    _subscribe_address_to_list(address, args.list_admin)
    # 3. inform owner about approval and subscription
    _inform_owner(address, domain, name)
    # 4. inform lists
    _inform_lists(address, domain, name)
