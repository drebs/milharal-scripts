#!/bin/sh

WP_ROOT=/var/sites/milharal/wordpress
WP="wp --allow-root --path ${WP_ROOT}"

usage() {
  echo "Usage: ${0} theme|plugin projectname"
}

# check command line
if [ ${#} -lt 2 ]; then
  usage
  exit 1
fi

cmd=${1}
proj=${2}

if [ "${cmd}" != "theme" -a "${cmd}" != "plugin" ]; then
  usage
  exit 1
fi

# install the required project
${WP} "${cmd}" install "${proj}"

# enable theme in multisite install
if [ "${cmd}" = "theme" ]; then
  ${WP} theme enable "${proj}" --network
fi

# fix permissions, because we run as root
chown -R milharal:www-data ${WP_ROOT}
