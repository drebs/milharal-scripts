#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2014 milharal.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import MySQLdb
import re

from argparse import ArgumentParser
from email.mime.text import MIMEText
from subprocess import Popen, PIPE


# locale configuration


WP_CONFIG_FILE = "/var/sites/milharal/site/wp-config.php"

ADMIN_ADDRESS = "milharal@lists.riseup.net"
ADMIN_SUBJECT = "Atenção: %d blogs estão parados na moderação!"
ADMIN_MESSAGE = u"""
Olá, essa é uma mensagem automática.

Os seguintes blogs estão aguardando moderação:

{pending_str}

Uma mensagem automática também foi enviada para os endereços de contato de cada
um dos blogs acima pedindo para que enviem uma descrição de seu projeto para o
seguinte endereço administrativo:

  {admin_address}

"""

BLOG_SUBJECT = "[milharal] Envie-nos uma mensagem para ativar %s !"
BLOG_MESSAGE = u"""
Olá!

Você solicitou o seguinte blog no milharal:

  {blog}

Para que possamos aprovar, por favor leia com atenção a nossa política de
hospedagem (https://milharal.org/politica/) e envie uma mensagem para
o seguinte endereço com uma breve descrição do seu projeto:

  {admin_address}

Até mais!
Equipe do milharal.org
"""


# database-related functions

def _get_db_info():
    info = {}
    p = re.compile("define\('DB_(HOST|NAME|USER|PASSWORD)', '([^']+)'\);")
    with open(WP_CONFIG_FILE, "r") as f:
        for l in f.readlines():
            m = p.match(l)
            if m:
                name = m.groups()[0].lower()
                value = m.groups()[1]
                info[name] = value
    return info


def _get_cursor():
    info = _get_db_info()
    db = MySQLdb.connect(
        host=info['host'],
        user=info['user'],
        passwd=info['password'],
        db=info['name'])
    return db.cursor()


def _get_pending_blogs():
    query = """
    SELECT
      wp_users.user_email,
      wp_blogs.domain,
      wp_blogs.registered,
      wp_blogs.last_updated
    FROM
      wp_users,
      wp_blogs,
      wp_usermeta
    WHERE
      wp_users.ID = wp_usermeta.user_id
      AND wp_usermeta.meta_key =
        CONCAT('wp_', wp_blogs.blog_id,'_capabilities')
      AND wp_usermeta.meta_value LIKE '%administrator%'
      AND wp_blogs.deleted = '2'
    ORDER BY
      wp_blogs.registered,
      wp_blogs.last_updated;
    """
    c = _get_cursor()
    c.execute(query)
    return c.fetchall()


# mail-related functions

def _send_mail(address, subject, body, send, cc=""):
    if send:
        msg = MIMEText(body, "plain", "utf-8")
        msg["From"] = ADMIN_ADDRESS
        msg["To"] = address
        if cc:
            msg["Cc"] = cc
        msg["Subject"] = subject
        _send_msg(msg)
    else:
        _print_mail(address, subject, body, cc=cc)


def _send_msg(msg):
    p = Popen(["/usr/sbin/sendmail", "-t"], stdin=PIPE)
    p.communicate(msg.as_string())


def _print_mail(address, subject, body, cc=""):
        print "-" * 78
        print "From: %s" % ADMIN_ADDRESS
        print "To: %s" % address
        if cc:
            print "Cc: %s" % cc
        print "Subject: %s" % subject
        print ""
        print body
        print "-" * 78


# actual script functions

def _send_blog_message(address, blog, send):
    _send_mail(
        address=address,
        subject=BLOG_SUBJECT % blog,
        body=BLOG_MESSAGE.format(
            blog=blog,
            admin_address=ADMIN_ADDRESS),
        # cc=ADMIN_ADDRESS,
        send=send)


def _send_admin_message(pending, send):
    _send_mail(
        address=ADMIN_ADDRESS,
        subject=ADMIN_SUBJECT % len(pending),
        body=ADMIN_MESSAGE.format(
            pending_str=_get_pending_blogs_str(pending),
            admin_address=ADMIN_ADDRESS),
        send=send)


def _send_blog_messages(pending, send):
    for address, blog, _, _ in pending:
        _send_blog_message(address, blog, send)


def _get_pending_blogs_str(pending):
    lines = []
    for address, blog, registered, _ in pending:
        lines.append("  blog: " + blog)
        lines.append("  email: " + address)
        lines.append("  registrado em: " + str(registered))
        # lines.append("atualizado em: " + str(last_updated))
        lines.append("")
    return "\n".join(lines)


# utilities

def _parse_args():
    parser = ArgumentParser()
    parser.add_argument(
        "-a", "--admin", action="store_true",
        help="Send message for admin address.")
    parser.add_argument(
        "-b", "--blogs", action="store_true",
        help="Send message for blogs addresses.")
    parser.add_argument(
        "-s", "--send", action="store_true",
        help="Really send, instead of just printing messages to stdout.")
    return parser.parse_args()


# and the program

if __name__ == "__main__":
    args = _parse_args()
    pending = _get_pending_blogs()
    if pending:
        if args.admin:
            _send_admin_message(pending, args.send)
        if args.blogs:
            _send_blog_messages(pending, args.send)
